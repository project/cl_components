<?php

namespace Drupal\cl_components\Exception;

/**
 * Raised when the component syntax in Twig is invalid.
 */
class ComponentSyntaxException extends \Exception {

}
